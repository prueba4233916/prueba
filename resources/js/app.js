require('./bootstrap');

window.Vue = require('vue').default;
window.axios = require('axios');

Vue.component('prueba-vue', require('./PruebaVue.vue').default);
Vue.component('prueba-sql', require('./PruebaSql.vue').default);


const app = new Vue({
    el: '#app',
});
