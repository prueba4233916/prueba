<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Producto;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 50; $i++) {
            $producto = new Producto;
            $producto->nombre = $faker->name();
            $producto->descripcion = $faker->text(100);
            $producto->precio = $faker->randomFloat(2,1,1000);
            $producto->save();
        }
    }
}
