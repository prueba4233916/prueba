<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\AtributoProducto;
use App\Models\Historial;
use App\Models\User;

class historialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $productos = AtributoProducto::get();
        $usuarios = User::get();

        foreach ($productos as $producto) {

            $user = $usuarios->random();
            
            $historial = new Historial;
            $historial->user_id = $user->id;
            $historial->producto_id = $producto->producto_id;
            $historial->atributo_id = $producto->atributo_id;
            $historial->valor_anterior = $faker->name();
            $historial->valor_nuevo = $faker->name();
            $historial->save();
        }
    }
}
