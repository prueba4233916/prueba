<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Producto;
use App\Models\Atributo;

class ProductosAtributosSeeder extends Seeder
{

    private $valores = [null, 'valor1', 'valor2', 'valor3'];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productos = Producto::all();
        $atributos = Atributo::all();

        foreach ($productos as $producto) {
            $cantidadRelaciones = rand(1, $atributos->count());
            $atributosAsignados = $atributos->random($cantidadRelaciones);

            foreach ($atributosAsignados as $atributo) {
                $producto->atributos()->attach($atributo, ['valor' => $this->getValorRandom()]);
            }

        }
    }


    /**
     * @return null|string
     */
    private function getValorRandom(): ?string
    {
        $key = array_rand($this->valores);
        return $this->valores[$key];
    }
}
