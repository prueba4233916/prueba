<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AtributoProducto extends Model
{
    use HasFactory;

    protected $table = 'atributo_producto';

    protected $fillable = [
        'producto_id',
        'atributo_id'
    ];

}
