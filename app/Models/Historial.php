<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    use HasFactory;

    protected $table = 'historial';

    protected $fillable = [
        'id_user',
        'id_producto',
        'id_atributo',
        'valor_anterior',
        'valor_nuevo'
    ];
}
