<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Atributo;
use App\Models\Producto;

class ConsultasController extends Controller
{
    
    public function getConsultas($id){

        $resultado = [];
        $sql = '';

        // Recuperar el usuario que más productos a editado en general, es decir, que ha editado sus atributos 
        if($id==1){
            $sql = "
                SELECT hs.user_id as id_usuario, us.name as nombre_usuario, hs.total_modificaciones
                FROM (
                    SELECT user_id, SUM(user_id) as total_modificaciones
                    FROM historial
                    GROUP BY user_id
                    ORDER BY total_modificaciones DESC
                    LIMIT 1
                ) hs
                INNER JOIN users us ON us.id = hs.user_id
            ";
        }


        // Recuperar el usuario que más productos a editado mirando un tipo concreto de atributo
        elseif($id==2){

            // Obtiene un atributo aleatorio
            $atributos = Atributo::get();
            $atributo = $atributos->random();

            $sql = "
                SELECT u.name as nombre_usuario, COUNT(h.producto_id) as productos_modificados
                FROM users u 
                INNER JOIN historial h ON h.user_id = u.id
                INNER JOIN atributos a ON a.id = h.atributo_id 
                WHERE a.nombre = '$atributo->nombre'
                GROUP BY u.name
                HAVING productos_modificados = (
                    SELECT COUNT(h.producto_id) as maximo
                    FROM historial h
                    INNER JOIN atributos a ON a.id = h.atributo_id 
                    WHERE a.nombre = '$atributo->nombre'
                    GROUP BY h.user_id 
                    ORDER BY maximo DESC 
                    LIMIT 1
                )
            ";
        }


        // Recuperar los productos que tienen un atributo concreto vacío
        elseif($id==3){

            // Obtiene un atributo aleatorio
            $atributos = Atributo::get();
            $atributo = $atributos->random();

            $sql = "
                SELECT p.id as id_producto, p.nombre as nombre_producto
                FROM productos p 
                INNER JOIN atributo_producto ap ON ap.producto_id = p.id 
                INNER JOIN atributos a ON a.id = ap.atributo_id 
                WHERE a.nombre = '$atributo->nombre'
                AND ap.valor IS NULL 
            ";
        }


        // Recuperar los productos que tienen algún atributo vacío
        elseif($id==4){
            $sql = "
                SELECT p.id as id_producto, p.nombre as nombre_producto
                FROM (
                    SELECT producto_id
                    FROM atributo_producto ap
                    WHERE valor IS NULL
                    GROUP BY producto_id 
                ) ap
                INNER JOIN productos p ON p.id =ap.producto_id
            ";
        }


        // Recuperar todos los atributos de un producto concreto
        elseif($id==5){

            // Obtiene un atributo aleatorio
            $productos = Producto::get();
            $producto = $productos->random();

            $sql = "
                SELECT p.nombre as nombre_producto, a.id as id_atributo, a.nombre as nombre_atributo, ap.valor 
                FROM atributo_producto ap 
                INNER JOIN atributos a ON a.id = ap.atributo_id 
                INNER JOIN productos p ON p.id = ap.producto_id 
                WHERE p.nombre = '$producto->nombre'
            ";
        }


        // Realiza la consulta
        $resultado = DB::select($sql);

        return [
            'resultado' => $resultado,
            'sql' => $sql
        ];
    }
}
